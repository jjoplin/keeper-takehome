resource "aws_instance" "app_instance" {
  count         = var.instance_count
  ami           = data.aws_ami.amzn2.id
  instance_type = var.instance_type
  key_name      = var.ssh_key_name
  vpc_security_group_ids = [
    aws_security_group.app_sg.id,
  ]
  # Spread the instances evenly across the provided subnets
  subnet_id            = element(var.private_subnet_ids, count.index % length(var.private_subnet_ids))
  iam_instance_profile = aws_iam_instance_profile.s3_readonly.name
  tags = {
    AnsibleRole = "${var.app_name}-worker"
    Name        = "${var.app_name}-worker-${count.index}"
    S3Bucket    = aws_s3_bucket.s3_bucket.bucket_domain_name
  }
}

resource "aws_security_group" "app_sg" {
  name        = "${var.app_name}-app-sg"
  description = "App instances security group"
  vpc_id      = var.vpc_id
}

resource "aws_security_group_rule" "app_sg_ingress" {
  from_port                = 80
  to_port                  = 80
  protocol                 = "tcp"
  security_group_id        = aws_security_group.app_sg.id
  type                     = "ingress"
  description              = "Allow HTTP from the load balancer"
  source_security_group_id = aws_security_group.lb_sg.id
}

resource "aws_security_group_rule" "app_sg_egress" {
  type              = "egress"
  from_port         = 0
  to_port           = 0
  protocol          = "-1"
  cidr_blocks       = ["0.0.0.0/0"]
  description       = "Allow full egress from app instances"
  security_group_id = aws_security_group.app_sg.id
}
