resource "aws_lb" "load_balancer" {
  name               = "${var.app_name}-load-balancer"
  internal           = false
  load_balancer_type = "application"
  security_groups = [
    aws_security_group.lb_sg.id
  ]
  subnets = var.public_subnet_ids
}

resource "aws_lb_target_group" "lb_target_group" {
  name = "${var.app_name}-lb-tg"
  port = 80
  health_check {
    matcher = "200-299,300-399"
  }
  protocol    = "HTTP"
  target_type = "instance"
  vpc_id      = var.vpc_id
}

resource "aws_lb_listener" "lb_listener" {
  load_balancer_arn = aws_lb.load_balancer.arn
  port              = 80
  protocol          = "HTTP"
  default_action {
    type             = "forward"
    target_group_arn = aws_lb_target_group.lb_target_group.arn
  }
}

resource "aws_security_group" "lb_sg" {
  name        = "allow_http"
  description = "Allow inbound http traffic"
  vpc_id      = var.vpc_id
}

resource "aws_security_group_rule" "lb_sg_ingress" {
  type              = "ingress"
  description       = "Allow ingress for HTTP traffic"
  from_port         = 80
  to_port           = 80
  protocol          = "tcp"
  cidr_blocks       = ["73.32.238.142/32"]
  security_group_id = aws_security_group.lb_sg.id
}

resource "aws_security_group_rule" "lb_sg_egress" {
  type              = "egress"
  description       = "Allow egress from the load balancer"
  from_port         = 0
  to_port           = 0
  protocol          = "-1"
  cidr_blocks       = ["0.0.0.0/0"]
  security_group_id = aws_security_group.lb_sg.id
}

resource "aws_lb_target_group_attachment" "lb_targets" {
  count            = var.instance_count
  target_group_arn = aws_lb_target_group.lb_target_group.arn
  target_id        = aws_instance.app_instance[count.index].id
}
