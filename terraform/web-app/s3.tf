resource "aws_s3_bucket" "s3_bucket" {
  bucket_prefix = "${var.app_name}-bucket"
  acl           = "public-read"
}

resource "aws_iam_instance_profile" "s3_readonly" {
  name = "${var.app_name}-s3-read-only"
  role = aws_iam_role.s3_readonly.name
}

resource "aws_iam_role" "s3_readonly" {
  name        = "${var.app_name}-read-s3-bucket"
  description = "Role to read from the S3 bucket"
  assume_role_policy = jsonencode({
    Version = "2012-10-17"
    Statement = [
      {
        Action = "sts:AssumeRole"
        Effect = "Allow"
        Sid    = ""
        Principal = {
          Service = "ec2.amazonaws.com"
        }
      },
    ]
  })
  managed_policy_arns = [
    aws_iam_policy.s3_readonly.arn
  ]
}

resource "aws_iam_policy" "s3_readonly" {
  name        = "${var.app_name}-s3-readonly"
  description = "Policy to read from the S3 bucket"
  policy      = data.aws_iam_policy_document.s3_readonly.json
}

data "aws_iam_policy_document" "s3_readonly" {
  statement {
    actions = [
      "s3:GetObject",
      "s3:ListBucket"
    ]
    resources = [
      "${aws_s3_bucket.s3_bucket.arn}/*",
      "${aws_s3_bucket.s3_bucket.arn}"
    ]
    effect = "Allow"
  }
}
