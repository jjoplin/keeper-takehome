variable "app_name" {
  type        = string
  description = "A name for the app"
}

variable "public_subnet_ids" {
  type        = list(string)
  description = "A list of public subnets to build the application in"
}

variable "private_subnet_ids" {
  type        = list(string)
  description = "A list of private subnets to build the application in"
}

variable "vpc_id" {
  type        = string
  description = "The ID of the VPC in which to create resources"
}

variable "instance_count" {
  type        = number
  description = "The number of app instances to build"
  default     = 2
}

variable "instance_type" {
  type        = string
  description = "The instance type to build out"
  default     = "t3.micro"
}

variable "ssh_key_name" {
  type        = string
  description = "SSH key to inject into the instance"
}
