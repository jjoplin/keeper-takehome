module "basic_vpc" {
  source = "./basic-vpc"
}

module "web-app" {
  source             = "./web-app"
  app_name           = "my-app"
  public_subnet_ids  = module.basic_vpc.public_subnet_ids
  private_subnet_ids = module.basic_vpc.private_subnet_ids
  vpc_id             = module.basic_vpc.vpc_id
  ssh_key_name       = module.basic_vpc.ssh_key_name
  instance_type      = "t3.micro"
}
