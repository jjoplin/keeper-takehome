terraform {
  required_version = "~>1.0.3"
  backend "s3" {
    bucket = "jjoplin-tf-state"
    key    = "main-vpc.tfstate"
    region = "us-east-1"
  }
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 3.0"
    }
  }
}
