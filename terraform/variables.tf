variable "region" {
  type        = string
  description = "Region in which to create AWS resources"
}
