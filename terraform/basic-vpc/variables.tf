variable "region" {
  default     = "us-east-1"
  type        = string
  description = "Region in which resources should be created"
}

variable "cidr" {
  default     = "10.0.0.0/18"
  type        = string
  description = "CIDR of the VPC"
}

variable "vpc_name" {
  default     = "prod-vpc"
  type        = string
  description = "Name of the VPC"
}

variable "public_subnet_count" {
  default     = 2
  type        = number
  description = "Number of public subnets"
}

variable "private_subnet_count" {
  default     = 2
  type        = number
  description = "Number of private subnets"
}

variable "rds_cluster_name" {
  default     = "aurora-cluster"
  type        = string
  description = "Default name for the cluster"
}

variable "rds_cluster_count" {
  default     = 3
  type        = number
  description = "Number of instances in the RDS Aurora cluster"
}

variable "rds_instance_type" {
  default     = "db.t3.medium"
  type        = string
  description = "Class of the instances in the Aurora cluster"
}

variable "rds_engine" {
  default     = "aurora-mysql"
  type        = string
  description = "Engine to use for Aurora cluster"
}

variable "rds_engine_version" {
  # This is the most recent patch release of the current LTS release of Aurora 2
  # https://docs.aws.amazon.com/AmazonRDS/latest/AuroraUserGuide/AuroraMySQL.Updates.2076.html
  default     = "5.7.mysql_aurora.2.07.6"
  type        = string
  description = "Specific engine version to use in the Aurora cluster"
}

variable "rds_port" {
  default     = 3306
  type        = number
  description = "Port that Aurora will use for comms"
}

variable "rds_subnet_group_name" {
  default     = "aurora-subnet-group"
  type        = string
  description = "Subnet group name for Aurora instances"
}

variable "rds_db_name" {
  default     = "dbname"
  type        = string
  description = "Name of the database on the Aurora cluster"
}

variable "rds_admin_username" {
  default     = "dbadmin"
  type        = string
  description = "Admin account on the Aurora cluster"
}

variable "rds_admin_password" {
  default     = "dbpassword1"
  type        = string
  description = "Admin password on the Auorora cluster"
}

variable "jump_host_type" {
  default     = "t3.micro"
  type        = string
  description = "Instance type for the jump host"
}
