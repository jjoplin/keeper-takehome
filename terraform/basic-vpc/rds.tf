resource "aws_rds_cluster" "rds_cluster" {
  cluster_identifier = var.rds_cluster_name
  engine             = var.rds_engine
  engine_version     = var.rds_engine_version
  database_name      = var.rds_db_name
  master_username    = var.rds_admin_username
  # In a production environment, this would link from an encrypted service
  # like Vault
  master_password = var.rds_admin_password
  # This would be true in production; for this exercise, I'm omitting it
  # for ease of teardown
  # deletion_protection  = true
  skip_final_snapshot = true

  # Also not setting a final snapshot identifer, which would be used in
  # production, since I don't want surprise charges on my credit card
  # final_snapshot_identifier = "${var.rds_cluster_name}-final-snapshot"
  storage_encrypted = true
  vpc_security_group_ids = [
    aws_security_group.aurora_sg.id,
  ]
  port                 = var.rds_port
  db_subnet_group_name = var.rds_subnet_group_name

  # Ignore AZ changes, since we are defaulting to 2 AZs instead of the
  # recommended 3. See
  # https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/rds_cluster#availability_zones
  lifecycle {
    ignore_changes = [
      availability_zones,
    ]
  }
}

resource "aws_rds_cluster_instance" "instance" {
  count                = var.rds_cluster_count
  identifier           = "${var.rds_cluster_name}-instance-${count.index}"
  cluster_identifier   = aws_rds_cluster.rds_cluster.id
  instance_class       = var.rds_instance_type
  engine               = aws_rds_cluster.rds_cluster.engine
  engine_version       = aws_rds_cluster.rds_cluster.engine_version
  db_subnet_group_name = var.rds_subnet_group_name
}

resource "aws_db_subnet_group" "aurora_subnet_group" {
  name       = var.rds_subnet_group_name
  subnet_ids = aws_subnet.private_subnet[*].id
  tags = {
    Name = var.rds_subnet_group_name
  }
}

resource "aws_security_group" "aurora_sg" {
  name        = "aurora-sg"
  description = "Security group allowing the VPC to access Aurora"
  vpc_id      = aws_vpc.vpc.id
}

resource "aws_security_group_rule" "aurora-ingress" {
  security_group_id = aws_security_group.aurora_sg.id
  description       = "Inbound access to RDS from the VPC"
  type              = "ingress"
  from_port         = var.rds_port
  to_port           = var.rds_port
  protocol          = "tcp"
  cidr_blocks = [
    var.cidr,
  ]
}

resource "aws_security_group_rule" "aurora_egress" {
  security_group_id = aws_security_group.aurora_sg.id
  description       = "Egress access from RDS"
  type              = "egress"
  from_port         = 0
  to_port           = 65535
  protocol          = "-1"
  cidr_blocks = [
    "0.0.0.0/0",
  ]
}
