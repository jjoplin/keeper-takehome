output "jump_host_ip" {
  value = aws_instance.jump_host.public_ip
}

output "public_subnet_ids" {
  value = aws_subnet.public_subnet[*].id
}

output "private_subnet_ids" {
  value = aws_subnet.private_subnet[*].id
}

output "vpc_id" {
  value = aws_vpc.vpc.id
}

output "ssh_key_name" {
  value = aws_key_pair.ssh_key.key_name
}
