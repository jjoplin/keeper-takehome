resource "aws_instance" "jump_host" {
  ami           = data.aws_ami.amzn2.id
  instance_type = var.jump_host_type
  tags = {
    Name        = "jump-host"
    AnsibleRole = "jump-host"
  }
  # In a production environment, this would be `false`, and would be
  # set as an EIP
  associate_public_ip_address = true
  key_name                    = aws_key_pair.ssh_key.key_name
  vpc_security_group_ids = [
    aws_security_group.ssh_jump_host.id
  ]
  subnet_id = aws_subnet.public_subnet[0].id
}

resource "aws_key_pair" "ssh_key" {
  key_name   = "default-ec2-key"
  public_key = "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAACAQDkxHCiu70XPsaH4nGVST7NwLRHU0rsdbsUlqwvHPvKSKFEgc5u55GYO1TDyy95S3Wd7bfAV8ZzcUbCjMv7bqB9Ts75UOAbGKwRkTQqpMsIHEvB9T4yq2kO7k1mI2P4pqECYSL8edM7Mi9kDeSYJHQDrNxsTpt+UHv+d4W9Msc/qdpnS+Jwlj69lUSh/rzH7+BNcS0A249zHnxTizXo13/0MsbaoTMDwb8WFI3fIyX35R1l5d6SobATl0H3Bla2TnGW+eLMzRScrMvYAPG7gz1ve+82k5NI9eKMxwoKSPGkh+tppsGKxPV1y4E7sxhXU++MZuAeN9NFZ/P7QqY8GvUwNSOvJLpCjHb0E/k3KqAoNWy3/RIdgqpdJjfhKhxwpH+VceM66rFEU1UkVHVr9PGyUc8yHbO9YB+Cjrsg5n7U2XS0Oo62Xh1ZMISkT8//7TPcDbPgvX5W/Bff0MRhNCziKWUE0GBK+s0k8WrlOTejoEqz0RhK/3SgzjChp7hQHhfGOSfeRBdc+Y43IqL64dGqK39j8E0OHklcdnykexFj72lOyFyH4ydiGUFBy5bzXbmssL1WBN37Se1k+SOJHdbLGPoh8pM6p345CclRrrmaD8pyq8a9ZRY2ZJqsGmWqNnSVu2HRj3D0ss+yZp8k8vLn/lnKMwFivIECzc48IM5oPw== jjoplin@jjoplin-fedora-v.justinjoplin.com"
}

resource "aws_security_group" "ssh_jump_host" {
  name        = "ssh-jump-host"
  description = "Allow SSH to the jump host"
  vpc_id      = aws_vpc.vpc.id
}

resource "aws_security_group_rule" "jump_ingress" {
  security_group_id = aws_security_group.ssh_jump_host.id
  type              = "ingress"
  description       = "SSH from my IP"
  from_port         = 22
  to_port           = 22
  protocol          = "tcp"
  cidr_blocks = [
    "73.32.238.142/32",
  ]
}

resource "aws_security_group_rule" "jump_egress" {
  security_group_id = aws_security_group.ssh_jump_host.id
  type              = "egress"
  description       = "Allow egress from jump host"
  from_port         = 0
  to_port           = 65535
  protocol          = "-1"
  cidr_blocks = [
    "0.0.0.0/0",
  ]
}
