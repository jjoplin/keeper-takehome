resource "aws_vpc" "vpc" {
  cidr_block = var.cidr
  tags = {
    Name = var.vpc_name
  }
}

resource "aws_subnet" "private_subnet" {
  count             = var.private_subnet_count
  vpc_id            = aws_vpc.vpc.id
  cidr_block        = cidrsubnet(var.cidr, 6, count.index)
  availability_zone = element(reverse(data.aws_availability_zones.azs.names), count.index)
  tags = {
    Name = "Private subnet ${count.index}"
  }
}

resource "aws_subnet" "public_subnet" {
  count             = var.public_subnet_count
  vpc_id            = aws_vpc.vpc.id
  availability_zone = element(reverse(data.aws_availability_zones.azs.names), count.index)
  # Add the public_subnet_count so we don't have IP conflicts
  cidr_block = cidrsubnet(var.cidr, 6, count.index + var.public_subnet_count)
  tags = {
    Name = "Public subnet ${count.index}"
  }
}

resource "aws_eip" "ngw_ip" {
  tags = {
    Name = "NatGW EIP"
  }
}

resource "aws_internet_gateway" "igw" {
  vpc_id = aws_vpc.vpc.id
  tags = {
    Name = "IGW for ${var.vpc_name}"
  }
}

resource "aws_route_table" "public_rt_tbl" {
  vpc_id = aws_vpc.vpc.id
  tags = {
    Name = "Public subnet route table"
  }
}

resource "aws_route" "public_route" {
  route_table_id         = aws_route_table.public_rt_tbl.id
  destination_cidr_block = "0.0.0.0/0"
  gateway_id             = aws_internet_gateway.igw.id
}

resource "aws_route_table_association" "public_rt_assn" {
  count          = var.public_subnet_count
  subnet_id      = aws_subnet.public_subnet[count.index].id
  route_table_id = aws_route_table.public_rt_tbl.id
}

resource "aws_nat_gateway" "ngw" {
  allocation_id = aws_eip.ngw_ip.id
  # Not truly HA, but since the number of public and private subnets aren't
  # guaranteed to match in this exercise, I'm just going to create 1 NAT
  # gateway and move on
  subnet_id = aws_subnet.public_subnet[0].id
  tags = {
    Name = "NGW for private subnets in ${var.vpc_name}"
  }
  depends_on = [aws_internet_gateway.igw]
}

resource "aws_route_table" "private_rt_tbl" {
  vpc_id = aws_vpc.vpc.id
  tags = {
    Name = "Private subnet route table"
  }
}

resource "aws_route" "private_route" {
  route_table_id         = aws_route_table.private_rt_tbl.id
  destination_cidr_block = "0.0.0.0/0"
  nat_gateway_id         = aws_nat_gateway.ngw.id
}

resource "aws_route_table_association" "private_rt_assn" {
  count          = var.private_subnet_count
  subnet_id      = aws_subnet.private_subnet[count.index].id
  route_table_id = aws_route_table.private_rt_tbl.id
}
